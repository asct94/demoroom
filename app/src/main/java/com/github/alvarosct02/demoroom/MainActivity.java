package com.github.alvarosct02.demoroom;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        updateCount();

        findViewById(R.id.bt_add).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                User user = new User();
                user.setFirstName("Alvaro");
                user.setLastName("Santa Cruz");
                AppDatabase.getInstance().userDao().insert(user);

                updateCount();
            }
        });


    }

    public void updateCount() {
        int count = AppDatabase.getInstance().userDao().getAll().size();
        ((TextView) findViewById(R.id.tv_count)).setText(String.valueOf(count));
    }
}
