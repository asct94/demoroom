package com.github.alvarosct02.demoroom;

import android.app.Application;

public class App extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        AppDatabase.initDb(getApplicationContext());
    }
}
