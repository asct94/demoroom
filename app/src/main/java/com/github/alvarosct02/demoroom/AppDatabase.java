package com.github.alvarosct02.demoroom;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

@Database(entities = {User.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {

    private static AppDatabase db;

    public static void initDb(Context ctx){
         db = Room.databaseBuilder(ctx,
                AppDatabase.class, "database-name")
                 .allowMainThreadQueries()
                 .build();
    }

    public static AppDatabase getInstance(){
        return db;
    }

    public abstract UserDao userDao();
}